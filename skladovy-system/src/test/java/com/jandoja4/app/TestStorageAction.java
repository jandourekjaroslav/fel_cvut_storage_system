/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.app;

import java.util.ArrayList;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author jarda
 */
public class TestStorageAction {
    
    public StorageActionController toTest;
    
    public TestStorageAction() {
    }
    
    
    @Before
    public void setUp() {
        toTest=new StorageActionController(new ArrayList<polozkaskladu>(), new ArrayList<skladtrans2>());
    }
    
    @After
    public void tearDown() {
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();
    
    @Test
    public void testNullrequests(){
        exception.expect(IllegalArgumentException.class);
        toTest.setRequests(null);
    
    
    }
    @Test 
    public void testNullitems(){
        exception.expect(IllegalArgumentException.class);
        toTest.setItems(null);
    
    
    }
    @Test
    public void testReturnItemsList(){
        ArrayList<polozkaskladu> expected=new ArrayList<polozkaskladu>();
        expected.add(new polozkaskladu());
        
        toTest.setItems(expected);
        assertEquals("Container hasn't set the long string properly", expected, toTest.getItems());
    }
    @Test 
    public void testReturnRequestList(){
        ArrayList<skladtrans2> expected=new ArrayList<skladtrans2>();
        toTest.setRequests(expected);
        assertEquals("Container hasn't set the long string properly", expected, toTest.getRequests());
    
    }
    @Test
    public void testWrongRequestList(){
        ArrayList expected=new ArrayList();
        expected.add("String");
        toTest.setRequests(expected);
        assertEquals("Container hasn't set the long string properly", expected, toTest.getRequests());
        
    }
   @Test
   public void testNullAdd(){
        exception.expect(IllegalArgumentException.class);
        toTest.addToStock(null, null, null, 0);
   }
}
