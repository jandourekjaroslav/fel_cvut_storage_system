
package com.jandoja4.app;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author jarda
 */
public class TestTellerAction {
    
    public TellerActionController toTest;
    
    public TestTellerAction() {
    }
    
    
    @Before
    public void setUp() {
        toTest=new TellerActionController(new ArrayList<polozkaskladu>());
    }
    
    @After
    public void tearDown() {
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();
    
    @Test
    public void testNullToSell(){
        exception.expect(IllegalArgumentException.class);
        toTest.setToSell(null);
    
    
    }
    @Test 
    public void testNullitems(){
        exception.expect(IllegalArgumentException.class);
        toTest.setItems(null);
    
    
    }
    @Test
    public void testReturnItemsList(){
        ArrayList<polozkaskladu> expected=new ArrayList<polozkaskladu>();
        expected.add(new polozkaskladu());
        
        toTest.setItems(expected);
        assertEquals("Container hasn't set the long string properly", expected, toTest.getItems());
    }
   
}
