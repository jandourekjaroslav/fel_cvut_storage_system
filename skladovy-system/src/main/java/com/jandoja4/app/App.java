package com.jandoja4.app;



/**
 * Main class of the application
 * holds all forms and controller class instances, 
 * functions as a bridge between view and model classes 
 * 
 * 
 * @author jarda
 */
public class App 
{
    private static LoginFrame loginDis ; //loginForm
    private static TallerForm tallerDis; //tallerForm
    private static StorageManagerForm storageDis;//storageManagement
    private static OrderForm stockDis;//stockManagement
    private static RoleSelect landPage;//Form selection
    private static loginController login;
    private static TellerLoadController tellerLoad;
    private static TellerActionController tellerAction;
    private static StorageLoadController storageLoad;
    private static StorageActionController storageAction;
    private static OrderActionController orderAction;
    private static OrderDisplayController orderDisplay;
    
    /**
     * Creates instances for all the forms and Display controlers
     * 
     */
     public static void createForms(){
        loginDis =new LoginFrame();
        tallerDis=new TallerForm();
        storageDis=new StorageManagerForm();
        stockDis=new OrderForm();
        landPage=new RoleSelect();
        tellerLoad=new TellerLoadController();
        storageLoad=new StorageLoadController();
        orderDisplay=new OrderDisplayController();
        
        
    
    }
     
     /**
      * main class of the program, runs createForms and shows login Form
      * @param args 
      */
    public static void main( String[] args )
    {
        createForms();
        loginDis.ShowLogin();
        
        
        
    }
    /**
     * Getter for the ActionController of Order frame-ordering of goods  
     * @return 
     */
    public static OrderActionController getOrderAction() {
        return orderAction;
    }
/**
 * Setter for ActionController of Order frame-ordering goods
 * @param orderAction 
 */
    public static void setOrderAction(OrderActionController orderAction) {
        App.orderAction = orderAction;
    }
/**
 * Getter for DisplayControler of Order frame- display functions
 * @return 
 */
    public static OrderDisplayController getOrderDisplay() {
        return orderDisplay;
    }
/**
 * Setter for DisplayController of Order frame
 * @param orderDisplay 
 */
    public static void setOrderDisplay(OrderDisplayController orderDisplay) {
        App.orderDisplay = orderDisplay;
        
    }
/**
 * Getter for DisplayController of Storage form
 * @return 
 */
    
    public static StorageLoadController getStorageLoad() {
        return storageLoad;
    }
/**
 * Getter for ActionController of Storage form 
 * @return 
 */
    public static StorageActionController getStorageAction() {
        return storageAction;
    }
/**
 * Setter for ActionController of Storage form 
 * @param storageAction 
 */
    public static void setStorageAction(StorageActionController storageAction) {
        App.storageAction = storageAction;
    }
/**
 * Getter for AcitonController of Teller form
 * @return 
 */
    public static TellerActionController getTellerAction() {
        return tellerAction;
    }
/**
 * Setter for ActionController of Teller Form
 * 
 * @param tellerAction 
 */
    public static void setTellerAction(TellerActionController tellerAction) {
        App.tellerAction = tellerAction;
    }
/**
 * Setter for LoginController 
 * @param login 
 */
    public static void setLogin(loginController login) {
        App.login = login;
    }
/**
 * Getter for LogginController
 * @return 
 */
    public static loginController getLogin() {
        return login;
    }

 /**
  * Getter for Teller Form
  * @return 
  */
    public static TallerForm getTallerDis() {
        return tallerDis;
    }
/**
 * Getter for Login frame
 * @return 
 */
    public static LoginFrame getLoginDis() {
        return loginDis;
    }
/**
 * Getter for Storage form
 * @return 
 */
    public static StorageManagerForm getStorageDis() {
        return storageDis;
    }
/**
 * Getter for Order form
 * @return 
 */
    public static OrderForm getStockDis() {
        return stockDis;
    }
/**
 * Getter for LandingPage form
 * @return 
 */
    public static RoleSelect getLandPage() {
        return landPage;
    }
/**
 * Getter for DisplayController of Teller form
 * @return 
 */
    public static TellerLoadController getTellerLoad() {
        return tellerLoad;
    }
    
   /**
    * Getter for Teller form
    * @return 
    */
    public TallerForm getTallerForm(){
        return tallerDis;
        
    }
    /**
     * Getter for Order form
     * @return 
     */
    public OrderForm getOrderForm (){
        return stockDis;
    }
    /**
     * Getter for Storage form
     * @return 
     */
    public StorageManagerForm getStorageManagerForm(){
        return storageDis;
    }
    /**
     * Getter for LandingPage form
     * @return 
     */
    public RoleSelect getLandingPage(){
        return landPage;
    }
    /**
     * Function to display LandingPage, hide all others
     * Calls reloadData
     */
    public static void showLandingPage(){
        loginDis.setVisible(false);
        tallerDis.setVisible(false);
        storageDis.setVisible(false);
        stockDis.setVisible(false);
        landPage.setVisible(true);
        reloadData();
    }
    /**
     * Function to create and start threads that load database data 
     */
    public static void reloadData(){
        
        LoadThreads teller=new LoadThreads("tellerThread", 1);
        teller.start();
        LoadThreads storage=new LoadThreads("storageThread", 2);
        storage.start();
        LoadThreads order=new LoadThreads("orderThread", 3);
        order.start();
        
    }
    /**
     * Function to display TellerForm and hide all others 
     */
    public static void showTellerForm(){
        loginDis.setVisible(false);
        tallerDis.setVisible(true);
        storageDis.setVisible(false);
        stockDis.setVisible(false);
        landPage.setVisible(false);
        
        
        
    
    }
    /**
     * Function to display StorageFormand hide all others 
     */
    public static void showStorageForm(){
        loginDis.setVisible(false);
        tallerDis.setVisible(false);
        storageDis.setVisible(true);
        stockDis.setVisible(false);
        landPage.setVisible(false);
    
    }
    /**
     * Function to display OrderForm and hide all others 
     */
    public static void showOrderForm (){
        loginDis.setVisible(false);
        tallerDis.setVisible(false);
        storageDis.setVisible(false);
        stockDis.setVisible(true);
        landPage.setVisible(false);
    
    }
    /**
     * Funciton to exit out of the app  
     */
    public static void terminateApp(){
        System.exit(0);
    
    }
}
