/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.app;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.swing.DefaultListModel;

/**
 * Class controlling display of Teller form
 * @author jarda
 */


public class TellerLoadController {
    private static String login;
    private static Integer amountOfStock=0;
   
   /**
    * function loading data for teller form from database 
    */ 
  public static void loadFramesPokladna(){
        DefaultListModel modelTeller=new DefaultListModel();
        EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        ArrayList<polozkaskladu> fullList = new ArrayList<polozkaskladu>();
        EntityManager entityManager= emfactory.createEntityManager();
        login=App.getLogin().getUsername();
        
        Query queryGetBuilding=entityManager.createQuery("Select e.sklad from zamestnanec e where e.rodnecislo = '"+login+"'");
        String mybuilding =(String) queryGetBuilding.getSingleResult();
        Query queryBuilding=entityManager.createQuery("Select e.id from budovaskladu e where e.sklad = '"+mybuilding+"'");
        List<String> listBuilding=queryBuilding.getResultList();
        
        for (String building:listBuilding){
                StoredProcedureQuery query = entityManager.createStoredProcedureQuery("pocetpolozek");
                query.registerStoredProcedureParameter("sklad", String.class, ParameterMode.IN);
                query.registerStoredProcedureParameter("total", Integer.class, ParameterMode.OUT);
            
                query.setParameter("sklad",String.valueOf(building));

                query.execute();
                amountOfStock+=(Integer) query.getOutputParameterValue("total");
                
               
                
                
                Query queryPolozky=entityManager.createQuery("Select e from polozkaskladu e where e.budova = '"+building+"'");
                List<polozkaskladu> polozkyList=queryPolozky.getResultList();
                
                for (polozkaskladu polo:polozkyList){
                    modelTeller.addElement(polo.getNazev() +"     "+ polo.getAmount());
                    fullList.add(polo);
                    
                
                }
        
        }
        App.setTellerAction(new TellerActionController(fullList));
        entityManager.close();
        emfactory.close();
        App.getTallerDis().getStorageOutput().setModel(modelTeller);
        App.getTallerDis().getAmoutOfUnique().setText("Amount of unique items:"+amountOfStock);

    
  
  }
  /**
   * Function ro reload SellFrame after succesfull insert into requests  
   */
  public static void reloadToSellFrame(){
      DefaultListModel modelToSell=new DefaultListModel();
      for (polozkaskladu polozka:App.getTellerAction().getToSell()){
          modelToSell.addElement(polozka.getNazev()+ " " + polozka.getAmount());
      
      }
      App.getTallerDis().getAmoutOfUnique().setText("Amount of unique items:"+amountOfStock);
      App.getTallerDis().getToSell().setModel(modelToSell);
  
  }
}
