
package com.jandoja4.app;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * Class controlling actions of Teller Form 
 * @author jarda
 */
public class TellerActionController {
 private ArrayList <polozkaskladu> items=new ArrayList<polozkaskladu>();
 private ArrayList <polozkaskladu> toSell=new ArrayList<polozkaskladu>();
/**
 * Constructor of ActionControllr class takes a List of all items in users storage as parametr 
 * @param items 
 */
 public TellerActionController(ArrayList <polozkaskladu> items){
     this.items=items;
 
 }
/**
 * Getter for Sell list
 * @return 
 */
    public ArrayList<polozkaskladu> getToSell() {
        return toSell;
    }
/**
 * Setter for Sell list
 * @param toSell 
 */
    public void setToSell(ArrayList<polozkaskladu> toSell) {
        if (toSell!=null){
        this.toSell = toSell;
        }else{ throw new IllegalArgumentException();
                
        }
    }
    /**
     * Adds item to Sell list by index position
     * @param index 
     */
    public void addToSell(int index){
        if(!(toSell.contains(items.get(index)))){
        toSell.add(items.get(index));
        TellerLoadController.reloadToSellFrame();
        }
    }
    /**
     * Changes amount of item in To sell list 
     * @param index
     * @param newamount 
     */
    public void changeAmount(int index, int newamount){
       if(index>-1){
        toSell.get(index).setAmount(newamount);
        TellerLoadController.reloadToSellFrame();
       }
    }
    /**
     * Adds a new Transaction request for every item in toSell list 
     */
    public void addTransaction(){
        EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        EntityManager entityManager= emfactory.createEntityManager();
        entityManager.getTransaction().begin();
        for (polozkaskladu pol:toSell){
            skladtrans2 trans = new skladtrans2();
            trans.setPolozka(pol.getIdproduktu());
            trans.setBudova(pol.getBudova());
            trans.setAkce("Prodej");
            trans.setStatus("Open");
            trans.setAmount(pol.getAmount());
            entityManager.persist(trans);
        }
        entityManager.getTransaction().commit();
        entityManager.close();
        emfactory.close();
        setToSell(new ArrayList<polozkaskladu>());
        TellerLoadController.reloadToSellFrame();
        
    }
    /**
     * getter for Items in  storagge 
     * @return 
     */
    public List<polozkaskladu> getItems() {
        return items;
    }
/**
 * Setter for Items lits of storage 
 * @param items 
 */
    public void setItems(ArrayList<polozkaskladu> items) {
        
        if(items!=null){
        this.items = items;
        
        }else{ throw new IllegalArgumentException();
                
        }
    }
 
}
