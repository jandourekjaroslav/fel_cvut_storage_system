/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.app;

import javax.swing.JLabel;
import javax.swing.JList;

/**
 *
 * @author jarda
 */
public class TallerForm extends javax.swing.JFrame {

    /**
     * Creates new form TallerForm
     */
    public TallerForm() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addToOrderButton = new javax.swing.JButton();
        amountToAdd = new javax.swing.JSlider();
        jScrollPane2 = new javax.swing.JScrollPane();
        itemsToAdd = new javax.swing.JList();
        jScrollPane1 = new javax.swing.JScrollPane();
        storageOutput = new javax.swing.JList();
        toAddItemsLabel = new javax.swing.JLabel();
        amountLabel = new javax.swing.JLabel();
        changeAmountButton = new javax.swing.JButton();
        backButt = new javax.swing.JButton();
        amoutOfUnique = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        addToOrderButton.setText("Order");
        addToOrderButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addToOrderButtonMouseClicked(evt);
            }
        });

        amountToAdd.setMajorTickSpacing(10);
        amountToAdd.setMinorTickSpacing(1);
        amountToAdd.setValue(0);

        itemsToAdd.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(itemsToAdd);

        storageOutput.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                storageOutputValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(storageOutput);

        toAddItemsLabel.setLabelFor(itemsToAdd);
        toAddItemsLabel.setText("Items to Order");

        amountLabel.setLabelFor(amountToAdd);
        amountLabel.setText("Amount");

        changeAmountButton.setText("Change Amount");
        changeAmountButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                changeAmountButtonMouseClicked(evt);
            }
        });

        backButt.setText("Back");
        backButt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backButtMouseClicked(evt);
            }
        });

        amoutOfUnique.setText("Amount of unique items: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backButt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(amoutOfUnique, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(14, 14, 14)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(toAddItemsLabel)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(amountLabel)
                                .addComponent(amountToAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(changeAmountButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addToOrderButton, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(toAddItemsLabel)
                        .addComponent(backButt))
                    .addComponent(amoutOfUnique, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(amountLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addComponent(amountToAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(changeAmountButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(addToOrderButton)
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void storageOutputValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_storageOutputValueChanged
       App.getTellerAction().addToSell(storageOutput.getSelectedIndex());
       
    }//GEN-LAST:event_storageOutputValueChanged

    private void changeAmountButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeAmountButtonMouseClicked
        App.getTellerAction().changeAmount(itemsToAdd.getSelectedIndex(), amountToAdd.getValue());
    }//GEN-LAST:event_changeAmountButtonMouseClicked

    private void addToOrderButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addToOrderButtonMouseClicked
        App.getTellerAction().addTransaction();
        
    }//GEN-LAST:event_addToOrderButtonMouseClicked

    private void backButtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backButtMouseClicked
        App.showLandingPage();
    }//GEN-LAST:event_backButtMouseClicked
    /**
     * getter for toSell
     * @return 
     */
    public JList getToSell(){
        return itemsToAdd;
    }
    /**
     * getter for Storage Output
     * @return 
     */
    public JList getStorageOutput() {
        return storageOutput;
    }
/**
 * geter for amount of unique 
 * @return 
 */
    public JLabel getAmoutOfUnique() {
        return amoutOfUnique;
    }
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addToOrderButton;
    private javax.swing.JLabel amountLabel;
    private javax.swing.JSlider amountToAdd;
    private javax.swing.JLabel amoutOfUnique;
    private javax.swing.JButton backButt;
    private javax.swing.JButton changeAmountButton;
    private javax.swing.JList itemsToAdd;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList storageOutput;
    private javax.swing.JLabel toAddItemsLabel;
    // End of variables declaration//GEN-END:variables
}
