/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.app;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

/**
 *
 * @author jarda
 */


@Entity
public class polozkaskladu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String idproduktu;
    private String umisteni;
    private String budova;
    private String trvandlivost;
    private String nazev;
    private int amount;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getUmisteni() {
        return umisteni;
    }

    public void setUmisteni(String umisteni) {
        this.umisteni = umisteni;
    }

    public String getBudova() {
        return budova;
    }

    public void setBudova(String budova) {
        this.budova = budova;
    }

    public String getTrvandlivost() {
        return trvandlivost;
    }

    public void setTrvandlivost(String trvandlivost) {
        this.trvandlivost = trvandlivost;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }
    

    public String getIdproduktu() {
        return idproduktu;
    }

    public void setIdproduktu(String idproduktu) {
        this.idproduktu = idproduktu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproduktu != null ? idproduktu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof polozkaskladu)) {
            return false;
        }
        polozkaskladu other = (polozkaskladu) object;
        if ((this.idproduktu == null && other.idproduktu != null) || (this.idproduktu != null && !this.idproduktu.equals(other.idproduktu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jandoja4.app.polozkaskladu[ id=" + idproduktu + " ]";
    }
    
}
