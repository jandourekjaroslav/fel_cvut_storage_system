package com.jandoja4.app;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.DefaultListModel;
/**
 * Class that controls all display functions of OrderForm
 * @author jarda
 */
public class OrderDisplayController {
    /**
     * Login of User
     */
    private static String login ;
    /**
     * Function to load all request for OrderForm(Requests marked OrdRequest) from database 
     */
    public static void loadOrderData(){
    
        DefaultListModel modelTrans=new DefaultListModel();
        
        ArrayList<transakce> requestList=new ArrayList<transakce>();

        EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        EntityManager entityManager= emfactory.createEntityManager();
        
                
                
        login=App.getLogin().getUsername();
        Query queryGetSklad=entityManager.createQuery("Select e.sklad from zamestnanec e where e.rodnecislo = '"+login+"'");
        String mujsklad =(String) queryGetSklad.getSingleResult();
        Query queryBudova=entityManager.createQuery("Select e.id from budovaskladu e where e.sklad = '"+mujsklad+"'");
        List<String> listbudovy=queryBudova.getResultList();
        for (String budova:listbudovy){
              
                Query queryPozadavky=entityManager.createQuery("Select e from transakce e where e.budova = '"+budova+"'");
                List<transakce> listPozadavky=queryPozadavky.getResultList();
                for (transakce e:listPozadavky){
                    if(e.getStatus().equals("Open")&&(e.getAkce().equals("OrdRequest"))){
                        polozkaskladu polos=entityManager.find(polozkaskladu.class, e.getPolozka());
                        modelTrans.addElement(e.getTransactionid()+"  "+polos.getNazev()+"  "+e.getAmount()+"  "+e.getAkce()+"  "+e.getStatus());
                        requestList.add(e);
                    }
                }
                

               
        
        }
        entityManager.close();
        emfactory.close();
        App.setOrderAction(new OrderActionController(requestList));
        App.getStockDis().getTransactionList().setModel(modelTrans);
        
        
    
    
    }
}
