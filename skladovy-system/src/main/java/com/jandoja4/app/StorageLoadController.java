
package com.jandoja4.app;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;

/**
 * Load controler for Storage Form
 * @author jarda
 */
public class StorageLoadController {
    private static String login;
/**
 * Function to load data for Storage Frame form database 
 */
    public static void loadFields() {
        DefaultListModel modelStorage=new DefaultListModel();
        DefaultListModel modelTrans=new DefaultListModel();
        DefaultComboBoxModel modelBuilding=new DefaultComboBoxModel();
        
        ArrayList<polozkaskladu> storageList=new ArrayList<polozkaskladu>();
        ArrayList<skladtrans2> requestList=new ArrayList<skladtrans2>();

        EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        EntityManager entityManager= emfactory.createEntityManager();
        
                
                
        login=App.getLogin().getUsername();
        Query queryGetStorage=entityManager.createQuery("Select e.sklad from zamestnanec e where e.rodnecislo = '"+login+"'");
        String myStorage =(String) queryGetStorage.getSingleResult();
        Query queryBuilding=entityManager.createQuery("Select e.id from budovaskladu e where e.sklad = '"+myStorage+"'");
        List<String> listBuildings=queryBuilding.getResultList();
        for (String building:listBuildings){
                modelBuilding.addElement(building);
                Query queryPozadavky=entityManager.createQuery("Select e from skladtrans2 e where e.budova = '"+building+"'");
                List<skladtrans2> listPozadavky=queryPozadavky.getResultList();
                for (skladtrans2 e:listPozadavky){
                    polozkaskladu polos=entityManager.find(polozkaskladu.class, e.getPolozka());
                    modelTrans.addElement(e.getTransactionid()+"  "+polos.getNazev()+"  "+e.getAmount()+"  "+e.getAkce()+"  "+e.getStatus());
                    requestList.add(e);
                }
                

                Query queryItems=entityManager.createQuery("Select e from polozkaskladu e where e.budova = '"+building+"'");
                List<polozkaskladu> itemsList=queryItems.getResultList();
                for (polozkaskladu polo:itemsList){
                    modelStorage.addElement(polo.getNazev() +"    "+ polo.getAmount()+"   "+"Building: "+polo.getBudova()+"  "+"Storage space: "+polo.getUmisteni());
                    storageList.add(polo);
                
                }
        
        }
        entityManager.close();
        emfactory.close();
        App.setStorageAction(new StorageActionController(storageList, requestList));
        App.getStorageDis().getStorageBuildingSelect().setModel(modelBuilding);
        App.getStorageDis().getActiveRequests().setModel(modelTrans);
        App.getStorageDis().getCurrentStock().setModel(modelStorage);
    }
    /**
     * Function to zero out textfields after succesfull Item Add 
     */
    public void reloadAddframes(){
        App.getStorageDis().getLocal().setText("");
        App.getStorageDis().getAddedItemName().setText("");
        App.getStorageDis().getAmount().setText("");
    
    }
  
  
}
