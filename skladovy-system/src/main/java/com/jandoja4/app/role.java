/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.app;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author jarda
 */
@Entity
public class role implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String nazev;

    public String getId() {
        return nazev;
    }

    public void setId(String id) {
        this.nazev = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nazev != null ? nazev.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof role)) {
            return false;
        }
        role other = (role) object;
        if ((this.nazev == null && other.nazev != null) || (this.nazev != null && !this.nazev.equals(other.nazev))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jandoja4.app.role[ id=" + nazev + " ]";
    }
    
}
