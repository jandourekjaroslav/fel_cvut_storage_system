package com.jandoja4.app;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.lang.IllegalArgumentException;

/**
 * Action controller for Storage Frame 
 * @author jarda
 */
public class StorageActionController {
     private ArrayList <polozkaskladu> items=new ArrayList<polozkaskladu>();
     private ArrayList <skladtrans2> requests=new ArrayList<skladtrans2>();
/**
 * Constructor for StorageActionController 
 * requres List of buildings and requests Called in the Display Controller where the data for this is loaded 
 * @param buildings
 * @param requests 
 */
    public StorageActionController(ArrayList <polozkaskladu> buildings, ArrayList<skladtrans2> requests) {
        requests=requests;
        items=buildings;
    
    }
/**
 * Getter for Items
 * @return 
 */
    public ArrayList<polozkaskladu> getItems() {
        return items;
    }
/**
 * Setter for Items 
 * @param items 
 */
    public void setItems(ArrayList<polozkaskladu> items) {
        if (items!=null){
        this.items = items;
        }else{ throw new IllegalArgumentException();
                
        }
        
        }
/**
 * Getter for Requests
 * @return 
 */
    public ArrayList<skladtrans2> getRequests() {
        return requests;
    }
/**
 * Setter for Requests 
 * @param requests 
 */
    public void setRequests(ArrayList<skladtrans2> requests) {
        if (requests!=null){
        this.requests = requests;}
        else{ throw new IllegalArgumentException();
                
        }
    }
    
    /**
     * 
     * Function to add new Item to database 
     * requiers 
     * @param name
     * @param building
     * @param storageSpace
     * @param amount 
     */
    public void addToStock(String name,String building,String storageSpace,int amount){
       if (name!=null && building!=null && storageSpace!=null){
     EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        EntityManager entityManager= emfactory.createEntityManager();
        entityManager.getTransaction().begin();
        
        polozkaskladu polozka =new polozkaskladu();
        polozka.setNazev(name);
        polozka.setBudova(building);
        polozka.setUmisteni(storageSpace);
        polozka.setAmount(amount);
        polozka.setTrvandlivost("00.00.000");
        entityManager.persist(polozka);
        entityManager.getTransaction().commit();
        entityManager.close();
        emfactory.close();
        App.getStorageLoad().reloadAddframes();
        StorageLoadController.loadFields();
       }else{ throw new IllegalArgumentException();
                
        }
       
       System.out.println("lalalalalal");
    }
    /**
     * Fuction to create new request for Order /add new request to database for item at index place in list 
     * @param index 
     */
    public void placeOrderRequest(int index){
        EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        EntityManager entityManager= emfactory.createEntityManager();
        entityManager.getTransaction().begin();
        skladtrans2 newTrans =new skladtrans2();
        newTrans.setBudova(items.get(index).getBudova());
        newTrans.setPolozka(items.get(index).getIdproduktu());
        newTrans.setAkce("OrdRequest");
        newTrans.setStatus("Open");
        newTrans.setAmount(100);
         entityManager.persist(newTrans);
        entityManager.getTransaction().commit();
        entityManager.close();
        emfactory.close();
       
    
    }
    /**
     * Function to resolve a request for storage / change status of Request and amout of items in stock/database on index place in list  
     * @param index 
     */
    public void resolveRequest(int index){
         EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        EntityManager entityManager= emfactory.createEntityManager();
        entityManager.getTransaction().begin();
        
        requests.get(index).setStatus("Closed");
        Query queryPolozky=entityManager.createQuery("UPDATE skladtrans2 SET status='Closed' WHERE transactionid="+requests.get(index).getTransactionid());
        queryPolozky.executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        if (requests.get(index).getAkce().equals("Prodej")){
            polozkaskladu alterPolozka= entityManager.find(polozkaskladu.class,requests.get(index).getPolozka());
            alterPolozka.setAmount(alterPolozka.getAmount()- requests.get(index).getAmount());
            
            
        
        
        }
        if(requests.get(index).getAkce().equals("OrdRequest")){
            polozkaskladu alterPolozka= entityManager.find(polozkaskladu.class,requests.get(index).getPolozka());
            alterPolozka.setAmount(alterPolozka.getAmount()+ requests.get(index).getAmount());
        
        }
        
        entityManager.getTransaction().commit();
        entityManager.close();
        emfactory.close();
        StorageLoadController.loadFields();

    }
}
