/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.app;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author jarda
 */
@Entity
public class skladovyukon implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String typukonu;
    private String pridelenarole;

    public String getTypukonu() {
        return typukonu;
    }

    public void setTypukonu(String typukonu) {
        this.typukonu = typukonu;
    }

    public String getPridelenarole() {
        return pridelenarole;
    }

    public void setPridelenarole(String pridelenarole) {
        this.pridelenarole = pridelenarole;
    }
    
}
