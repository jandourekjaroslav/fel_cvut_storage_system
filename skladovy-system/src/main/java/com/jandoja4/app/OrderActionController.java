

package com.jandoja4.app;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * Class controling all Action behavior coming from OrderForm
 * @author jarda
 */
public class OrderActionController {
    /**
     * List of all loaded requests
     */
    private ArrayList<transakce> requests;
    /**
     * Constructor for OrderActionController class
     * @param trans 
     */
    public  OrderActionController(ArrayList <transakce> trans){
        requests=trans;
    
    }
    /**
     * Function to resolve request given by index coming from 
     * @param index 
     * 
     */
     public void resolveRequest(int index){
         EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        EntityManager entityManager= emfactory.createEntityManager();
        entityManager.getTransaction().begin();
        
        requests.get(index).setStatus("Pending");
        Query queryPolozky=entityManager.createQuery("UPDATE transakce SET status='Pending' WHERE transactionid="+requests.get(index).getTransactionid());
        queryPolozky.executeUpdate();
        
        
        
        entityManager.getTransaction().commit();



        entityManager.close();
        emfactory.close();
        OrderDisplayController.loadOrderData();
            
        
        
     }
        
    
}
