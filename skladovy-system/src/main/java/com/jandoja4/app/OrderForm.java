package com.jandoja4.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
/**
 * Displaying elements to control Ordering side of the application 
 * @author jarda
 */
public class OrderForm extends javax.swing.JFrame {
    private JList transactionList;
    private JScrollPane listScroller ;
    private JButton resolveButton ;
    private JPanel listPanel;
    private JPanel buttonPanel;
    private JButton createOrderButton;
    private JButton backButt;
    /**
     * Constructor for OrderForm
     */
    public OrderForm(){
         setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
         setMinimumSize(new Dimension(698, 427));
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        //this.getContentPane().add(transactionList);
       
        this.listPanel=new JPanel();
        this.buttonPanel=new JPanel();
        createLists();
        createButtons();
       setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
        getContentPane().add(listPanel);
        getContentPane().add(buttonPanel);
        createListeners();
       
    }
    /**
     * Function to Create all the buttons
     */
    private void createButtons(){
        this.resolveButton=new JButton("Resolve Request");
        this.createOrderButton=new JButton("Create Order Summary");
        this.backButt=new JButton("Back");
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        
        
        this.buttonPanel.add(createOrderButton);
        this.buttonPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        this.buttonPanel.add(resolveButton);
        this.buttonPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        this.buttonPanel.add(backButt);
        this.buttonPanel.setBorder(BorderFactory.createEmptyBorder(265, 0,0, 10));
    
    }
    /**
     * Function to create all the EventListeners
     */
    private void createListeners(){
        backButt.addActionListener(new backListener());
        createOrderButton.addActionListener(new createListener(this));
        resolveButton.addActionListener(new resolveListener());
    
    }
    /**
     * Listener for resolveRequest Button
     */
    private class resolveListener implements ActionListener{

        public void actionPerformed(ActionEvent ae) {
            App.getOrderAction().resolveRequest(transactionList.getSelectedIndex());
        }
    
    
    
    }
    /**
     * Listener for Back button
     * 
     */
    private class backListener implements ActionListener{
    
        public void actionPerformed(ActionEvent ae) {
            App.showLandingPage();

        }
    }
    /**
     * Listener for Create Summary button
     */
    private class createListener implements ActionListener{
        
        private OrderForm parent;
        public createListener(OrderForm parent){
            this.parent=parent;
        }

        public void actionPerformed(ActionEvent ae) {
            JOptionPane.showMessageDialog(parent, "Order Summary Created (not reall)");
        }
    
    }
/**
 * getter for ListPanel
 * @return 
 */
    public JPanel getListPanel() {
        return listPanel;
    }

    /**
     * Setter for ListPanel
     * @param listPanel 
     */
    public void setListPanel(JPanel listPanel) {
        this.listPanel = listPanel;
    }
    /**
     * Function to create the List in this form
     */
    private void createLists(){
        this.transactionList=new JList();
        JLabel requestLabel=new JLabel("Active Requests");
        this.transactionList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        this.transactionList.setLayoutOrientation(JList.VERTICAL);
        this.transactionList.setVisibleRowCount(-1);
        this.listScroller =  new JScrollPane(transactionList);
        this.listScroller.setPreferredSize(new Dimension(450, 320));
        DefaultListModel model =new DefaultListModel();
        model.addElement("ABCD");
        this.transactionList.setModel(model);
        this.listPanel.add(requestLabel);
        this.listPanel.add(this.listScroller);
        
        
        
        
    }

    public JList getTransactionList() {
        return transactionList;
    }
    
    
}
