/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.app;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author jarda
 */
@Entity
public class zamestnanec implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String rodnecislo;
    private String telefon;
    private String jmenoaprijmeni;
    private String sklad;
    private String heslo;

    public String getHeslo() {
        return heslo;
    }

    public void setHeslo(String heslo) {
        this.heslo = heslo;
    }
    
    public String getRodnecislo() {
        return rodnecislo;
    }

    public void setRodnecislo(String rodnecislo) {
        this.rodnecislo = rodnecislo;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getJmenoaprijmeni() {
        return jmenoaprijmeni;
    }

    public void setJmenoaprijmeni(String jmneoaprijmeni) {
        this.jmenoaprijmeni = jmneoaprijmeni;
    }

    public String getSklad() {
        return sklad;
    }

    public void setSklad(String sklad) {
        this.sklad = sklad;
    }

   
    
}
