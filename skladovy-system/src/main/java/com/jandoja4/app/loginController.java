
package com.jandoja4.app;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

/**
 *Class controling login of user 
 * @author jarda
 */
public class loginController {
    private String username;
    private String password;
/**
 * Constructor for Login controller 
 * @param uname
 * @param pass 
 */
    public loginController(String uname,char[] pass) {
        username=uname;
        password=new String(pass);
            
    }
    /**
     * Checks Login id and password against the employee database 
     */
    public void checkLogin(){
    
        EntityManagerFactory emfactory=Persistence.createEntityManagerFactory("persunit");
        EntityManager entityManager= emfactory.createEntityManager();
        zamestnanec ucet =entityManager.find(zamestnanec.class, username);
        if (ucet!=null){
            if(ucet.getHeslo().equals(password)){
                
                App.showLandingPage();

            }else{App.getLoginDis().showPopup("Wrong Password");}
            
            
            
        }else{App.getLoginDis().showPopup("Wrong Username");}
        entityManager.close();
        emfactory.close();
    
    }
/** 
 * Getter for username 
 * @return 
 */
    public String getUsername() {
        return username;
    }
    
    
    
    
    
}
