/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.app;

/**
 * Class determining thread behavior 
 * @author jarda
 */
public class LoadThreads implements Runnable{
    private String threadName;
    private Thread thread;
    private Integer task;
/**
 * Constructor of LoadThreads 
 * Requires name of Thread
 * @param name
 * Requires number of task (index of form to load for)
 * @param task 
 */
    public LoadThreads(String name,Integer task) {
        this.threadName=name;
        this.task=task;
    }
    
    /**
     * Body of every thread, by assignet task loads one of three Frame data from Database
     */
    public void run() {
        //try {
         switch (task){
            case 1:
                TellerLoadController.loadFramesPokladna();

                break;
            case 2:
                StorageLoadController.loadFields();

                break;
                
            case 3:    
                OrderDisplayController.loadOrderData();
                 
                break;
        
     //   }
         }
      //catch (InterruptedException e) {
        // System.out.println("Thread " +  threadName + " interrupted.");
      //}
       

    }
    /**
     * Function to start Thread if empty
     */
    public void start(){
        if (thread == null) {
         thread = new Thread (this, threadName);
         thread.start ();
        }
    }
}
