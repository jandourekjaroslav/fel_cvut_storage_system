package com.jandoja4.app;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(zamestnanec.class)
public abstract class zamestnanec_ {

	public static volatile SingularAttribute<zamestnanec, String> heslo;
	public static volatile SingularAttribute<zamestnanec, String> sklad;
	public static volatile SingularAttribute<zamestnanec, String> rodnecislo;
	public static volatile SingularAttribute<zamestnanec, String> jmenoaprijmeni;
	public static volatile SingularAttribute<zamestnanec, String> telefon;

}

