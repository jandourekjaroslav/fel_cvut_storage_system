package com.jandoja4.app;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(transakce.class)
public abstract class transakce_ {

	public static volatile SingularAttribute<transakce, String> akce;
	public static volatile SingularAttribute<transakce, Integer> amount;
	public static volatile SingularAttribute<transakce, String> budova;
	public static volatile SingularAttribute<transakce, String> polozka;
	public static volatile SingularAttribute<transakce, Long> transactionid;
	public static volatile SingularAttribute<transakce, String> status;

}

