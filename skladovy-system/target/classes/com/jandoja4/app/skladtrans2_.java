package com.jandoja4.app;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(skladtrans2.class)
public abstract class skladtrans2_ {

	public static volatile SingularAttribute<skladtrans2, String> akce;
	public static volatile SingularAttribute<skladtrans2, Integer> amount;
	public static volatile SingularAttribute<skladtrans2, String> budova;
	public static volatile SingularAttribute<skladtrans2, String> polozka;
	public static volatile SingularAttribute<skladtrans2, Long> transactionid;
	public static volatile SingularAttribute<skladtrans2, String> status;

}

