package com.jandoja4.app;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(polozkaskladu.class)
public abstract class polozkaskladu_ {

	public static volatile SingularAttribute<polozkaskladu, Integer> amount;
	public static volatile SingularAttribute<polozkaskladu, String> budova;
	public static volatile SingularAttribute<polozkaskladu, String> nazev;
	public static volatile SingularAttribute<polozkaskladu, String> umisteni;
	public static volatile SingularAttribute<polozkaskladu, String> idproduktu;
	public static volatile SingularAttribute<polozkaskladu, String> trvandlivost;

}

